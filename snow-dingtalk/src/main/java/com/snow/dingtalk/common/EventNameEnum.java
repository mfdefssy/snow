package com.snow.dingtalk.common;

/**
 * @author qimingjin
 * @Title:
 * @Description:
 * @date 2020/11/5 15:04
 */
public enum EventNameEnum {

    org_dept_create,

    org_dept_modify,

    org_dept_remove
}
