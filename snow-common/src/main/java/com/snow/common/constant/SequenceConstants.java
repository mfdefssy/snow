package com.snow.common.constant;

/**
 * @author qimingjin
 * @Title: 序列号通用常量
 * @Description:
 * @date 2020/11/23 16:39
 */
public class SequenceConstants {
    /**
     *请假单
     */
    public static final String OA_LEAVE_SEQUENCE = "OA_QJ";

    /**
     * 采购单
     */
    public static final String OA_PURCHASE_SEQUENCE="OA_CG";

    /**
     * 离职单
     */
    public static final String OA_RESIGN_SEQUENCE="OA_LZ";

    /**
     * FAQ单
     */
    public static final String OA_FAQ_SEQUENCE="OA_FAQ";

    /**
     * 邮件编码
     */
    public static final String OA_EMAIL_SEQUENCE="OA_YJ";


    public static final String OA_CUSTOMER_SEQUENCE="OA_KH";
}
